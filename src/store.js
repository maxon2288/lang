import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import { SERVER_URL } from '@/constants'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isLogin: false,
    token: '',
    phones: [],
    email: '',
    skype: '',
    fa: false,
    go: false,
    vk: false,
    instagram: false,
    od: false,
    lat: 0,
    lng: 0,
    resp: {
      userType: 'lol',
      name: 'asdasd'
    },
    rasp: [],
    editRasp: false
  },
  plugins: [createPersistedState()],
  getters: {
    getVk: state => {
      return state.vk
    },
    getPhones: state => {
      return state.phones
    },
    getEmail: state => {
      return state.email
    },
    getSkype: state => {
      return state.skype
    },
    getFa: state => {
      return state.fa
    },
    getInstagram: state => {
      return state.instagram
    },
    getOd: state => {
      return state.od
    },
    getGo: state => {
      return state.go
    },
    getLat: state => {
      return state.lat
    },
    getLng: state => {
      return state.lng
    },
    isLogin: state => {
      return state.isLogin
    },
    getToken: state => {
      return state.token
    },
    getResp: state => {
      return state.resp
    },
    getRasp: state => {
      return state.rasp
    },
    getEditRasp: state => {
      return state.editRasp
    }
  },
  mutations: {
    setPhones (state, phones) {
      state.phones = phones
    },
    setEmail (state, email) {
      state.email = email
    },
    setSkype (state, skype) {
      state.skype = skype
    },
    setFa (state, facebook) {
      state.fa = facebook
    },
    setVk (state, vkontakte) {
      state.vk = vkontakte
    },
    setInstagram (state, instagram) {
      state.instagram = instagram
    },
    setOd (state, odnoklassniki) {
      state.od = odnoklassniki
    },
    setGo (state, google) {
      state.go = google
    },
    setLat (state, lat) {
      state.lat = lat
    },
    setLng (state, lng) {
      state.lng = lng
    },
    autorize (state, token) {
      state.isLogin = true
      state.token = token
    },
    logout (state) {
      state.isLogin = false
      state.token = ''
      state.resp = {userType: 'lol'}
    },
    setResp (state, resp) {
      state.resp = resp
    },
    setRasp (state, resp) {
      state.rasp = resp
    },
    addRasp (state, rasp) {
      state.rasp.push(rasp)
    },
    chageRasp (state) {
      if (state.editRasp === false) {
        state.editRasp = true
      } else {
        state.editRasp = false
      }
    }
  },
  actions: {
    chageRaspp ({commit}) {
      commit('chageRasp')
    },
    setContacts ({commit}) {
      axios.get(SERVER_URL + 'contacts').then(response => {
        commit('setPhones', response.data.data.contacts.phones)
        commit('setEmail', response.data.data.contacts.email)
        commit('setSkype', response.data.data.contacts.skype)

        commit('setLat', response.data.data.contacts.map.lat)
        commit('setLng', response.data.data.contacts.map.lng)
        commit('setFa', response.data.data.contacts.facebook)
        commit('setGo', response.data.data.contacts.google)
        commit('setVk', response.data.data.contacts.vkontakte)
        commit('setInstagram', response.data.data.contacts.instagram)
        commit('setOd', response.data.data.contacts.odnoklassniki)
      }).catch(e => {
        console.log(e)
      })
    },
    autorize ({commit}, token) {
      commit('autorize', token)
      axios
        .get(SERVER_URL + 'profile', {
          headers: { 'Authorization': this.getters.getToken }
        })
        .then(response => {
          let resp = response.data.data.profile
          for (let x = 0; x < resp.roles.length; x++) {
            if (resp.roles[x].id === 3) {
              resp.userType = 'student'
            } else if (resp.roles[x].id === 2) {
              resp.userType = 'teacher'
            }
          }
          if (resp.userType === 'anonimus') {
            resp.userType = false
          }
          commit('setResp', resp)
        })
        .catch(e => {
          console.log(e)
        })
    },
    setRasp ({commit}, newx) {
      let arr = this.getters.getRasp;
      arr.push(newx);
      let data = {schedule: JSON.stringify(arr), _method: "put"}
      data = JSON.stringify(data)
      axios.post(SERVER_URL + 'profile/schedule', data, { headers: { 'Authorization': this.getters.getToken, "Content-Type": "application/json" } })
        .then(response => {
          console.log(response);
        })
        .catch(error => {
          console.log(error)
        })
    },
    setTimeRasp ({commit}, newx) {
      let data = {schedule: JSON.stringify(newx), _method: "put"}
      data = JSON.stringify(data)
      axios.post(SERVER_URL + 'profile/schedule', data, { headers: { 'Authorization': this.getters.getToken, "Content-Type": "application/json" } })
        .then(response => {
          commit('setRasp', newx)
        })
        .catch(error => {
          console.log(error)
        })
    },
    userInfo ({commit}) {
      axios
        .get(SERVER_URL + 'profile', {
          headers: { 'Authorization': this.getters.getToken }
        })
        .then(response => {
          let resp = response.data.data.profile
          for (let x = 0; x < resp.roles.length; x++) {
            if (resp.roles[x].id === 3) {
              resp.userType = 'student'
            } else if (resp.roles[x].id === 2) {
              resp.userType = 'teacher'
            }
          }
          if (resp.userType === 'anonimus') {
            resp.userType = false
          }
          if (resp.schedule) {
            commit('setRasp', resp.schedule)
          }
          commit('setResp', resp)
        })
        .catch(e => {
          console.log(e)
        })
    },
    logout ({commit}) {
      commit('logout')
    }
  }
})
