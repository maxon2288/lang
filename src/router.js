import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Reviews from './views/Reviews'
import Faq from './views/Faq'
import Rates from './views/Rates'
import Specialists from './views/Specialists'
import Articles from './views/Articles'
import NotFound from './views/NotFound'
import Article from './views/Article'
import Examination from './views/Examination'
import Contacts from './views/Contacts'
import Login from './views/Login'
import store from './store'
import User from './views/User'
import Register from './views/Register'
import Lessons from './views/Lessons'
import Leksika from './views/Leksika'
import Readers from './views/Readers'
import Gramms from './views/Gramms'
import Vebinars from './views/Vebinars'
import Videos from './views/Videos'
import Reader from './views/Reader'
import Obratka from './views/Obratka'
import Chat from './views/Chat'
import Chat2 from './views/Chat2'
import Gram from './views/Gram'
import About from './views/About'
import Settings from './views/Settings'
import Modules from './views/Modules'
import Lesson from './views/Lesson'
import Urok from './views/Urok'
import Test from './views/Test'
import Tasks from './views/Tasks'
import Task from './views/Task'
import Playment from './views/Playment'
import Thanks from './views/Thanks'
import Polit from './views/Polit'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  },
  routes: [
    {
      path: '/polit',
      name: 'polit',
      component: Polit
    },
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/reviews',
      name: 'Отзывы',
      component: Reviews
    },
    {
      path: '/faq',
      name: 'Частые вопросы',
      component: Faq
    },
    {
      path: '/rates',
      name: 'Трифы',
      component: Rates
    },
    {
      path: '/articles',
      name: 'Полезные статьи',
      component: Articles
    },
    {
      path: '/specialists',
      name: 'Специалисты',
      component: Specialists
    },
    {
      path: '/article/:slug',
      name: 'Статья',
      component: Article
    },
    {
      path: '/404',
      name: '404',
      component: NotFound
    },
    {
      path: '/examination/:examinationname',
      name: 'Examination',
      component: Examination
    },
    {
      path: '/contacts',
      name: 'Contacts',
      component: Contacts
    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    {
      path: '/login',
      name: 'Авторизация',
      component: Login,
      beforeEnter: (to, from, next) => {
        if (store.getters.isLogin) {
          next('/user')
        } else {
          next()
        }
      }
    },
    {
      path: '/register',
      name: 'Регистрация',
      component: Register,
      beforeEnter: (to, from, next) => {
        if (store.getters.isLogin) {
          next('/user')
        } else {
          next()
        }
      }
    },
    {
      path: '/user',
      name: 'Личный кабинет',
      component: User,
      beforeEnter: (to, from, next) => {
        if (store.getters.isLogin) {
          next()
        } else {
          next('/login')
        }
      }
    },
    {
      path: '/settings',
      name: 'Настройки',
      component: Settings,
      beforeEnter: (to, from, next) => {
        if (store.getters.isLogin) {
          next()
        } else {
          next('/login')
        }
      }
    },
    {
      path: '/lessons',
      name: 'Уроки',
      component: Lessons,
      beforeEnter: (to, from, next) => {
        if (store.getters.isLogin) {
          next()
        } else {
          next('/login')
        }
      }
    },
    {
      path: '/finish_payment',
      name: 'thanks',
      component: Thanks,
      beforeEnter: (to, from, next) => {
        if (store.getters.isLogin) {
          next()
        } else {
          next('/login')
        }
      }
    },
    {
      path: '/lesson/:id',
      name: 'Урокс',
      component: Lesson,
      beforeEnter: (to, from, next) => {
        if (store.getters.isLogin) {
          next()
        } else {
          next('/login')
        }
      }
    },
    {
      path: '/tasks/:id',
      name: 'Проверка уроков ученика',
      component: Tasks,
      beforeEnter: (to, from, next) => {
        if (store.getters.isLogin) {
          next()
        } else {
          next('/login')
        }
      }
    },
    {
      path: '/student/:idStud/task/:id',
      name: 'Проверка уроков ученика',
      component: Task,
      beforeEnter: (to, from, next) => {
        if (store.getters.isLogin) {
          next()
        } else {
          next('/login')
        }
      }
    },
    {
      path: '/lessons/:id',
      name: 'Modules',
      component: Modules,
      beforeEnter: (to, from, next) => {
        if (store.getters.isLogin) {
          next()
        } else {
          next('/login')
        }
      }
    },
    {
      path: '/urok/:id',
      name: 'Urok',
      component: Urok,
      beforeEnter: (to, from, next) => {
        if (store.getters.isLogin) {
          next()
        } else {
          next('/login')
        }
      }
    },
    {
      path: '/playment/:id',
      name: 'Playment',
      component: Playment,
      beforeEnter: (to, from, next) => {
        if (store.getters.isLogin) {
          next()
        } else {
          next('/login')
        }
      }
    },
    {
      path: '/leksika',
      name: 'Лексика',
      component: Leksika,
      beforeEnter: (to, from, next) => {
        if (store.getters.isLogin) {
          next()
        } else {
          next('/login')
        }
      }
    },
    {
      path: '/reads',
      name: 'Чтение',
      component: Readers,
      beforeEnter: (to, from, next) => {
        if (store.getters.isLogin) {
          next()
        } else {
          next('/login')
        }
      }
    },
    {
      path: '/reads/:id',
      name: 'Чтение внутренняя',
      component: Reader,
      beforeEnter: (to, from, next) => {
        if (store.getters.isLogin) {
          next()
        } else {
          next('/login')
        }
      }
    },
    {
      path: '/gramms/:id',
      name: 'Грамматика внутреняя',
      component: Gram,
      beforeEnter: (to, from, next) => {
        if (store.getters.isLogin) {
          next()
        } else {
          next('/login')
        }
      }
    },
    {
      path: '/gramms',
      name: 'Грамматика',
      component: Gramms,
      beforeEnter: (to, from, next) => {
        if (store.getters.isLogin) {
          next()
        } else {
          next('/login')
        }
      }
    },
    {
      path: '/vebinars',
      name: 'Вебинары',
      component: Vebinars,
      beforeEnter: (to, from, next) => {
        if (store.getters.isLogin) {
          next()
        } else {
          next('/login')
        }
      }
    },
    {
      path: '/videos',
      name: 'Видео',
      component: Videos,
      beforeEnter: (to, from, next) => {
        if (store.getters.isLogin) {
          next()
        } else {
          next('/login')
        }
      }
    },
    {
      path: '/mail',
      name: 'Обратная свящь',
      component: Obratka,
      beforeEnter: (to, from, next) => {
        if (store.getters.isLogin) {
          next()
        } else {
          next('/login')
        }
      }
    },
    {
      path: '/chat',
      name: 'Чат',
      component: Chat,
      beforeEnter: (to, from, next) => {
        if (store.getters.isLogin) {
          next()
        } else {
          next('/login')
        }
      }
    },
    {
      path: '/chatx/:id',
      name: 'Чат учителя',
      component: Chat2,
      beforeEnter: (to, from, next) => {
        if (store.getters.isLogin) {
          next()
        } else {
          next('/login')
        }
      }
    },
    {
      path: '/test/:id',
      name: 'Тест',
      component: Test,
      beforeEnter: (to, from, next) => {
        if (store.getters.isLogin) {
          next()
        } else {
          next('/login')
        }
      }
    },
    {
      path: '/logout',
      name: 'Выход',
      beforeEnter: (to, from, next) => {
        store.dispatch('logout')
        next('/')
      }
    }
  ],
})
