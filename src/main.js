import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { sync } from 'vuex-router-sync'
import * as VueGoogleMaps from 'vue2-google-maps'
import Meta from 'vue-meta'
import { SERVER_URL } from "constants";
import $ from 'jquery'


router.beforeEach((to, from, next) => {
  console.log(to);
  var settings = {
    "url": "http://api.ie-exams.ru/api/v1/metas?url="+to.fullPath,
    "method": "GET",
    "timeout": 0,
    "headers": {
      "Accept": "application/json"
    },
  };
  $.ajax(settings).done(function (response) {
    console.log(response.data.meta.description);
    const title=response.data.meta.title;
    const description=response.data.meta.description;
    const keywords=response.data.meta.keywords;
    if (title) document.title = title;
    $('meta[name="description"]').attr('content',description);
    $('meta[name="keywords"]').attr('content',keywords);
  });
  
  next();
})

Vue.use(Meta, {
  keyName: 'metaInfo', // the component option name that vue-meta looks for meta info on.
  attribute: 'data-vue-meta', // the attribute name vue-meta adds to the tags it observes
  ssrAttribute: 'data-vue-meta-server-rendered', // the attribute name that lets vue-meta know that meta info has already been server-rendered
  tagIDKeyName: 'vmid' // the property name that vue-meta uses to determine whether to overwrite or append a tag
})
Vue.config.productionTip = false

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyBBzGdxzlxcbuMq6yp8Jo4XfUZQe2XMozs'
  }
})
sync(store, router)

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
